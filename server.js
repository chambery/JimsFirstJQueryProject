// var sqlite3 = require("sqlite3").verbose();
var express = require('express'),
    app = express();
var bodyParser = require("body-parser");
var methodOverride = require('method-override');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(methodOverride());
/* errs */
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

var dateFormat = require('dateformat');
var fs = require('fs');
var file = "../propmgmt.sqlite";
var exists = fs.existsSync(file);
console.log("file exists: " + exists);
// var db = new sqlite3.Database(file);
// console.log("db: " + db.toString());
YAML = require('yamljs');
var TAFFY = require('taffy');
carsdb = null;



function logErrors(err, req, res, next) {
  console.error(err.stack);
  next(err);
}

function clientErrorHandler(err, req, res, next) {
  if (req.xhr) {
    res.status(500).send({ error: 'Something blew up!' });
  } else {
    next(err);
  }
}

function errorHandler(err, req, res, next) {
  res.status(500);
  res.render('error', { error: err });
}

function populate_taffy(data) {

        carsdb = TAFFY(data);
        console.log(carsdb());
}

function make_dir(path)
{
    try
    {
        fs.lstatSync(path);
    } catch (e)
    {
        fs.mkdirSync(path, 0755);
    }
}


app.get('/autos', function (req, res) {
    console.log("in get makes");
    var makes = carsdb().map(function (rec, index)
    {
        console.log("rec: " + rec + "  rec.title: " + rec.title);

        return rec.title;
    });
    console.log("makes: " + makes)
    res.send(JSON.stringify(makes));
});

app.get('/autos/:make', function (req, res) {
    console.log("in get models for: " + req.params.make);
    carsdb().each(function(rec, index)
    {
        console.log(rec.title);
    });
    console.log(carsdb( {title: "Acura"}).first().models);
    var models = carsdb({title: req.params.make}).first().models
    console.log("models: " + models)
    res.send(JSON.stringify(models));
});


fs.readFile("/Users/chambery/projects/JimsFirstJQueryProject/makes-models.json", {"encoding": "utf8", "flag": "r"}, function(err, data)
{
            if(err) console.log(err);
            else populate_taffy(data);
});

app.use('/', express.static(__dirname));

app.listen(1860);

console.log('Express server started on port 1860');



